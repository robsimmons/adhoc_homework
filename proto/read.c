#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <inttypes.h>

/* Endian-ness handling. Semi-portable endian checks are from
 * https://gist.github.com/panzi/6856583 */
#if defined(__APPLE__)
#	include <libkern/OSByteOrder.h>
#	define be32toh(x) OSSwapBigToHostInt32(x)
#	define be64toh(x) OSSwapBigToHostInt64(x)
#else /* Pray it's Linux */
#	include <endian.h>
#endif

uint8_t read1(FILE *F) {
  assert(!feof(F));
  return fgetc(F);
}

uint32_t read4(FILE *F) {
  uint32_t netint;
  assert(fread(&netint, 4, 1, F));
  return be32toh(netint);
}

uint64_t read8(FILE *F) {
  uint64_t netint;
  assert(fread(&netint, 8, 1, F));
  return be64toh(netint);
}


/* MPS7 proprietary binary protocol format */

enum tag {
  DEBIT = 0,
  CREDIT = 1,
  START_AUTOPAY = 2,
  END_AUTOPAY = 3
};

struct record {
  enum tag tag;
  uint32_t timestamp;
  uint64_t userid;
  union {
    uint64_t asint;
    double asdouble;
  } amount;
};

void print_record (struct record *R) {
  time_t t = R->timestamp;
  char *s = ctime(&t);

  switch (R->tag) {
  case DEBIT:
    printf("Debit  %-10.9G for %19"PRIu64" %s", 
           R->amount.asdouble, R->userid, s);
    break;
    
  case CREDIT:
    printf("Credit %-10.9G for %19"PRIu64" %s", 
           R->amount.asdouble, R->userid, s);
    break;
    
  case START_AUTOPAY:
    printf("Begin autopay for %23"PRIu64" %s",
           R->userid, s);
    break;
    
  case END_AUTOPAY:
    printf("End autopay for   %23"PRIu64" %s",
           R->userid, s);
    break;
  }
}

// Used to sort array by users (not currently used)
int compare_user(const void *x, const void *y) {
  uint64_t a = ((struct record*)x)->userid;
  uint64_t b = ((struct record*)y)->userid;
  return a < b ? -1 : a > b;
}

// Used to sort array by timestamp (actually used)
int compare_timestamp(const void *x, const void *y) {
  uint64_t a = ((struct record*)x)->timestamp;
  uint64_t b = ((struct record*)y)->timestamp;
  return a < b ? -1 : a > b;
}

int main() {
  FILE *F = fopen("txnlog.dat", "rb");


  /* STEP 1: READ AND PROCESS HEADER */

  char buf[] = "1234"; // Length 5, adds the \'0'
  assert(fread(buf, 4, 1, F));
  assert(!strcmp("MPS7", buf));
  uint8_t version = read1(F);
  uint32_t numrec = read4(F);

  /* Experimentally, there appear to be numrec+1 records. Perhaps this
   * makes sense, because why would you have a header with no records?
   * It's certainly strange, though. This next line acconts for
   * this. */
  numrec++;

  printf("Reading version %d\nExpecting %d records\n\n", version, numrec);


  /* STEP 2: READ RECORDS */

  struct record *A = calloc(sizeof(struct record), numrec);
  assert(A != NULL);

  for (uint32_t i = 0; i < numrec; i++) {
    // Most fields appear every time
    A[i].tag = read1(F);
    A[i].timestamp = read4(F);
    A[i].userid = read8(F);

    // Tag-specific information
    switch(A[i].tag) {

    // In these cases, there's an extra field
    case CREDIT:
    case DEBIT:
      A[i].amount.asint = read8(F);
      break;

    // In these cases, the record is fully processed
    case START_AUTOPAY:
    case END_AUTOPAY:
      break;

    default:
      fprintf(stderr, "ERROR, UNKNOWN TAG: %d\n", A[i].tag);
      return 1;
    }
  }


  /* STEP 3: MAKE SURE WE'VE EXACTLY READ THE FILE; CLOSE FILE */

  assert(EOF == fgetc(F));
  fclose(F);


  /* OPTIONAL STEP 4: GRATUITIOUS SORTING BY TIMESTAMP */

  qsort(A, numrec, sizeof(struct record), &compare_timestamp);


  /* STEP 5: PASS FOR ANALYSIS */

  /* I am mystified by what the "debit" and "credit" amounts actually
   * mean, given that when I look at them they cleary aren't in
   * whole-cent amounts. */
  const uint64_t USER = 2456938384156277127;
  double debits = 0.0, credits = 0.0, user_balance = 0.0;
  uint32_t autopay_starts = 0, autopay_ends = 0;

  printf("All transactions of user %"PRIu64":\n", USER);
  for (uint32_t i = 0; i < numrec; i++) {
    switch (A[i].tag) {
    case CREDIT:
      credits += A[i].amount.asdouble;
      if (A[i].userid == USER) user_balance += A[i].amount.asdouble;
      break;

    case DEBIT:
      debits += A[i].amount.asdouble;
      if (A[i].userid == USER) user_balance -= A[i].amount.asdouble;
      break;

    case START_AUTOPAY:
      autopay_starts++;
      break;

    case END_AUTOPAY:
      autopay_ends++;
      break;
    }
    
    if (A[i].userid == USER) {
      print_record(&A[i]);
    }
  }

  free(A);

  
  /* STEP 6: PRINT OUT SUMMARY */

  printf("User's balance is: %-20.21G\n\n", user_balance);
  printf("Summary\n");
  printf("Total debits:     %10.11G\n", debits);
  printf("Total credits:    %10.11G\n", credits);
  printf("Autopays started: %u\n", autopay_starts);
  printf("Autopays ended:   %u\n", autopay_ends);

  return 0;
}
