import csv
from decimal import *

# This approach attempts to minimize memory usage. 

# First, we find all the zip codes we actually care about
zips = {}
with open('slcsp.csv', 'r') as slcspfile:
   for row in csv.DictReader(slcspfile):
      zips[row['zipcode']] = set()

# Next, we'll learn all the rate areas (a tuple (state, zone)) which
# each zip code overlaps. For each rate area that we care about, we'll
# plan to keep track of at most the two lowest-cost plans, initially
# set to None and None
rateAreas = {}
with open('zips.csv', 'r') as zipfile:
   for row in csv.DictReader(zipfile):
      rateArea = (row['state'], int(row['rate_area']))
      if row['zipcode'] in zips:
         zips[row['zipcode']].add(rateArea)
         rateAreas[rateArea] = (None, None)

# As we learn about plans we'll update our knowlege of the two
# lowest-cost silver plans in each relevant rate area
def merge(newplan, pair):
   lcsp, slcsp = pair
   if lcsp is None: return (newplan, None)
   if newplan['rate'] < lcsp['rate']: return (newplan, lcsp)
   if slcsp is None: return (lcsp, newplan)
   if newplan['rate'] < slcsp['rate']: return (lcsp, newplan)
   return (lcsp, slcsp)

with open('plans.csv', 'r') as planfile:
   for plan in csv.DictReader(planfile):
      rateArea = (plan['state'], int(plan['rate_area']))
      if plan['metal_level'] == 'Silver' and rateArea in rateAreas:
         plan['rate'] = Decimal(plan['rate'])
         rateAreas[rateArea] = merge(plan, rateAreas[rateArea])

# Finshing stage 1: only keep the SLCSP, if it exists, for each rate area
def slcspForRateArea(pair):
   lcsp, slcsp = pair
   if slcsp is None: return None
   return slcsp['rate']

for rateArea in rateAreas:
   rateAreas[rateArea] = slcspForRateArea(rateAreas[rateArea])

# Finishing stage 2: only keep the SLCSP, if it exists, for each zip code
def slcspForZip(areas):
   global rateAreas
   zipSLCSP = None
   for rateArea in areas:
      # Any rate area in the zip without a SLCSP --> failure
      if rateAreas[rateArea] is None: return None
      # Any two rate areas in the zip with different SLCSP --> failure
      if zipSLCSP is not None and zipSLCSP != rateAreas[rateArea]: return None
      zipSLCSP = rateAreas[rateArea]
   return zipSLCSP

for zipcode in zips:
   zips[zipcode] = slcspForZip(zips[zipcode])
         
# Now we'll step through the original file one more time, adding
# output to the output file
output = []
with open('slcsp.csv', 'r') as slcspfile:
   output.append(slcspfile.readline())
   for line in slcspfile.readlines():
      zipcode = line.split(',')[0]
      newline = zipcode + ','
      if zips[zipcode] is not None: newline += str(zips[zipcode])
      output.append(newline+'\n')

with open('slcsp.csv', 'w') as slcspfile:
   for line in output:
      slcspfile.write(line)

