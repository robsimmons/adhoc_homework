structure Analysis = 
struct


(*** REPRESENTING COSTS ***)

(* Because costs have finer-grained precision than cents (WHY?!), we 
 * can't do the standard trick of represent costs as whole-valued
 * pennies. Instead, we represent them as a pair of a number and
 * string, which has the advantage that lexicographic comparison of
 * the whole cost as an int + fractional part as a string does the
 * right thing. *)
type cost = {dollars: IntInf.int, cents: string}

fun parseInt str =
   case IntInf.fromString str of
      SOME i => i
    | _ => raise Fail ("Not a number: "^str)

fun parseCost cost = 
   case String.fields (fn x => x = #".") cost of
      [dollars] => {dollars = parseInt dollars, cents = ""}
    | [dollars, cents] => {dollars = parseInt dollars, cents = cents}
    | _ => raise Fail ("Not a cost: "^cost)

fun costToString {dollars, cents} = 
   case cents of 
      "" => IntInf.toString dollars
    | _ => IntInf.toString dollars ^ "." ^ cents

structure CostOrdered = 
struct 
   type t = {dollars: IntInf.int, cents: string}
   fun eq (x: t, y: t) = x = y
   fun compare (x: t, y: t) =
      (case IntInf.compare (#dollars x, #dollars y) of
          LESS => LESS
        | GREATER => GREATER
        | EQUAL => String.compare (#cents x, #cents y))
end

(*** READ PROVIDED CSV FILES ***)

(* Read in a CSV as a list, ignoring the CSV header *)
fun fgetcsv filename = 
let
   val file = TextIO.openIn filename
   val _ = TextIO.inputLine file (* Ignore CSV header *)
   fun loop () =
      case TextIO.inputLine file of
         NONE => [] before TextIO.closeIn file
       | SOME s => String.fields (fn x => x = #",") s :: loop ()
in
   loop ()
end

(* Get the relevant CSV files *)
val slcsp = fgetcsv "../slcsp.csv"
val geo = fgetcsv "../zips.csv"
val plan = fgetcsv "../plans.csv"



(*** LOAD FILES INTO DATABASE ***)

(* General-ish helper functions for parsing information *)
fun parseMetal str = 
   case str of 
      "Silver" => Metal.Silver
    | "Gold" => Metal.Gold
    | "Bronze" => Metal.Bronze
    | "Platinum" => Metal.Platinum
    | "Catastrophic" => Metal.Catastrophic  
    | _ => raise Fail ("Unknown metal: "^str)

(* Munge the geo database (zips.csv) into the right types *)
fun add_geo (fields, db) =
   case fields of 
      [zip, state, fips, name, rate_area] =>
      Healthcare.Assert.geo ((parseInt zip,
                              state,
                              parseInt fips,
                              name,
                              parseInt rate_area),
                             db)
    | _ => raise Fail ("Bad line (add_geo): "^String.concatWith "," fields)

(* Munge the plans database (plans.csv) into the right types *)
fun add_plan (fields, db) =
   case fields of 
      [plan_id, state, metal, rate, rate_area] =>
      Healthcare.Assert.plan ((plan_id,
                               state,
                               parseMetal metal,
                               rate,
                               parseInt rate_area),
                              db)
    | _ => raise Fail ("Bad line (add_plan): "^String.concatWith "," fields)

val db = foldl add_plan 
            (foldl add_geo Healthcare.empty geo)
            plan

(* Consistency checks must pass or this will fail (see healthcare.l10) *)
val [] = Healthcare.Query.inconsList db



(*** EXPLORE THE EXISTING DATA, PROBE CORNER CASES ***)

(* Record oddball states (states with no zip codes but listed plans,
 * or no plans but listed zip codes) *)

val oddballs = Healthcare.Query.oddballsList db
val () = print ("\nWeird missing/extraneous information\n");
val () = app (fn (state, msg) => print (state^": "^msg^"\n")) oddballs

(* Initialize data structures *)

structure ZipSet = IntInfRedBlackSet

structure PlanSet = 
RedBlackSet
(structure Elem =
 struct
    type t = string * cost
    fun eq (x: t, y: t) = x = y
    fun compare ((id1, cost1), (id2, cost2)) =
      (case CostOrdered.compare (cost1, cost2) of
          LESS => LESS
        | GREATER => GREATER
        | EQUAL => String.compare (id1, id2))
 end)

structure RateAreaSet = 
RedBlackSet 
(structure Elem = 
 struct
    type t = string * IntInf.int
    fun eq (x: t, y: t) = x = y
    fun compare ((state1, area1), (state2, area2)) = 
      (case String.compare (state1, state2) of
          LESS => LESS
        | GREATER => GREATER
        | EQUAL => IntInf.compare (area1, area2))
 end)

val allZips: ZipSet.set = 
   Healthcare.Query.allZips
      (fn (zip, set) => ZipSet.insert set zip)
      ZipSet.empty db

val allRateAreas: RateAreaSet.set = 
   Healthcare.Query.allRateAreas
      (fn (ratearea, set) => RateAreaSet.insert set ratearea)
      RateAreaSet.empty db

val getRateAreasForZip: IntInf.int -> RateAreaSet.set = 
   Healthcare.Query.lookupRateAreas
      (fn ((state, area), set) => RateAreaSet.insert set (state, area)) 
      RateAreaSet.empty db

val getSilverPlansForRateArea: string * IntInf.int -> PlanSet.set = 
   Healthcare.Query.lookupSilvers
     (fn ((id, cost), set) => PlanSet.insert set (id, parseCost cost)) 
     PlanSet.empty db

val getZipsForRateArea: string * IntInf.int -> ZipSet.set =
   Healthcare.Query.lookupZips
     (fn (zip, set) => ZipSet.insert set zip) 
     ZipSet.empty db

val getSLCSPForRateArea: string * IntInf.int -> (string * cost) option = 
   fn rate => 
      case PlanSet.toList (getSilverPlansForRateArea rate) of
         _ :: plan :: _ => SOME plan
       | _ => NONE

val getLCSPForRateArea: string * IntInf.int -> (string * cost) option = 
   fn rate => 
      case PlanSet.toList (getSilverPlansForRateArea rate) of
         plan :: _ => SOME plan
       | _ => NONE

fun SLCSPToString NONE = "No SCLSP"
  | SLCSPToString (SOME (id, cost)) = "SLCSP "^id^", cost "^costToString cost 

(* Tests for individual special and edge cases *)

val storedTests: (string * (string * IntInf.int -> bool)) list ref = ref [] 
fun rateAreaTest (desc, test) = 
 ( storedTests := !storedTests @ [ (desc, test) ]
 ; case List.find test (RateAreaSet.toList allRateAreas) of
      NONE => print ("\nNo rate area with property: "^desc^"\n")
    | SOME (state, area) => 
       ( print ("\nRate area "^state^"-"^
                IntInf.toString area^
                " ("^SLCSPToString (getSLCSPForRateArea (state, area))^
                ") has property:\n"^desc^"\n")
       ; case ZipSet.toList (getZipsForRateArea (state, area)) of
            [] => print "   No zip code corresponds to this rate area!\n"
          | zip :: _ => print ("   Example zip: "^IntInf.toString zip^"\n") ))

val storedZipTests: (string * (IntInf.int -> bool)) list ref = ref []
fun zipTest (desc, test) =
 ( storedZipTests := !storedZipTests @ [ (desc, test) ]
 ; case List.find test (ZipSet.toList allZips) of
      NONE => print ("\nNo zip code with propery: "^desc^"\n")
    | SOME zip =>
       ( print ("\nZip code "^IntInf.toString zip^" has property: "^desc^"\n") 
       ; List.app 
            (fn (state, area) =>
                print ("   Member of "^state^"-"^IntInf.toString area^", "^
                       SLCSPToString (getSLCSPForRateArea (state, area))^"\n"))
            (RateAreaSet.toList (getRateAreasForZip zip))))

fun isRateAreaWithNSilverPlans n ratearea = 
   n = PlanSet.size (getSilverPlansForRateArea ratearea)
val () = rateAreaTest ("0 silver plans", isRateAreaWithNSilverPlans 0)
val () = rateAreaTest ("1 silver plan", isRateAreaWithNSilverPlans 1)
val () = rateAreaTest ("2 silver plans", isRateAreaWithNSilverPlans 2)

fun isRateAreaWithPlansButNoSilverPlans ratearea = 
   isRateAreaWithNSilverPlans 0 ratearea
   andalso Healthcare.Query.hasSomePlan db ratearea
val () = rateAreaTest ("some plans, none are silver", 
                       isRateAreaWithPlansButNoSilverPlans)
   
fun isRateAreaWithTwoMincostPlans ratearea =
   case PlanSet.toList (getSilverPlansForRateArea ratearea) of 
      (_, cost1) :: (_, cost2) :: (_, cost3) :: _ => 
         cost1 = cost2 andalso cost2 <> cost3
    | _ => false
val () = rateAreaTest ("LCSP = SLCSP", isRateAreaWithTwoMincostPlans)

fun isRateAreaWithConfusableCostPlans ratearea =
   case PlanSet.toList (getSilverPlansForRateArea ratearea) of 
      (_, cost1) :: (_, cost2) :: _ => 
         (#dollars cost1 = #dollars cost2)
         andalso parseInt (#cents cost1) > parseInt (#cents cost2)
    | _ => false
val () = rateAreaTest ("LCSP and SLCSP confusable", 
                       isRateAreaWithConfusableCostPlans)

fun isRateAreaWithShortSLCSP ratearea = 
   case getSLCSPForRateArea ratearea of
      SOME (_, {cents = "", ...}) => true
    | _ => false
val () = rateAreaTest ("SLCSP is a whole number", isRateAreaWithShortSLCSP)

fun isRateAreaWithLongSLCSP ratearea = 
   case getSLCSPForRateArea ratearea of
      NONE => false
    | SOME (_, {cents, ...}) => size cents > 2
val () = rateAreaTest ("SLCSP not whole cents", isRateAreaWithLongSLCSP)

fun isZipInMultipleRateAreas zip = 
   case RateAreaSet.toList (getRateAreasForZip zip) of
      _ :: _ :: _ => true
    | _ => false
val () = zipTest ("multiple rate areas", isZipInMultipleRateAreas)

fun isZipInMultipleRateAreasSameSLCSPSameLCSP zip = 
   case RateAreaSet.toList (getRateAreasForZip zip) of
      [ ratearea1, ratearea2 ] =>
         isSome (getSLCSPForRateArea ratearea1)
         andalso isSome (getSLCSPForRateArea ratearea2)
         andalso #2 (valOf (getSLCSPForRateArea ratearea1)) =
                 #2 (valOf (getSLCSPForRateArea ratearea2))
         andalso #2 (valOf (getLCSPForRateArea ratearea1)) =
                 #2 (valOf (getLCSPForRateArea ratearea2))
    | _ => false 
val () = zipTest ("two rate areas, 1 SLCSP, same LCSP", 
                  isZipInMultipleRateAreasSameSLCSPSameLCSP)

fun isZipInMultipleRateAreasSameSLCSPDifferentLCSP zip = 
   case RateAreaSet.toList (getRateAreasForZip zip) of
      [ ratearea1, ratearea2 ] =>
         isSome (getSLCSPForRateArea ratearea1)
         andalso isSome (getSLCSPForRateArea ratearea2)
         andalso #2 (valOf (getSLCSPForRateArea ratearea1)) =
                 #2 (valOf (getSLCSPForRateArea ratearea2))
         andalso #2 (valOf (getLCSPForRateArea ratearea1)) <>
                 #2 (valOf (getLCSPForRateArea ratearea2))
    | _ => false 
val () = zipTest ("two rate areas, 1 SLCSP, different LCSP",
                  isZipInMultipleRateAreasSameSLCSPDifferentLCSP)

fun isZipInMultipleStatesSameAreaNoPlans zip = 
   case RateAreaSet.toList (getRateAreasForZip zip) of
      (state, area) :: (plans as _ :: _) =>
         List.all (fn ratearea => not (isSome (getSLCSPForRateArea ratearea)))
            ((state, area) :: plans)
         andalso List.all (fn (_, area') => area = area') plans
    | _ => false
val () = zipTest ("many rate areas, same area part, no plans",
                  isZipInMultipleStatesSameAreaNoPlans)

fun isZipInMultipleStatesSameAreaSomePlans zip = 
   case RateAreaSet.toList (getRateAreasForZip zip) of
      (state, area) :: (plans as _ :: _) =>
         List.all (fn (_, area') => area = area') plans
         andalso 
         List.exists 
            (fn ratearea => isSome (getSLCSPForRateArea ratearea)) 
            ((state, area) :: plans)
         andalso
         List.exists 
            (fn ratearea => not (isSome (getSLCSPForRateArea ratearea))) 
            ((state, area) :: plans)
    | _ => false
val () = zipTest ("many rate areas, same area part, some have plans",
                  isZipInMultipleStatesSameAreaSomePlans)

fun isZipInMultipleStatesSameAreaAllPlans zip = 
   case RateAreaSet.toList (getRateAreasForZip zip) of
      (state, area) :: (plans as _ :: _) =>
         List.all (fn ratearea => isSome (getSLCSPForRateArea ratearea))
            ((state, area) :: plans)
         andalso List.all (fn (_, area') => area = area') plans
    | _ => false
val () = zipTest ("many rate areas, same area part, all have plans",
                  isZipInMultipleStatesSameAreaAllPlans)

(* Analyize a given zip code *)
fun process zip =
let
   val rateareas = RateAreaSet.toList (getRateAreasForZip (parseInt zip))
in
 ( print (zip^",")
 ; case map getSLCSPForRateArea rateareas of
      SOME (_, cost) :: plans => 
         if List.all (fn NONE => false | SOME (_, cost') => cost = cost') plans
            then print (costToString cost^"\n")
         else print "\n"
    | _ => print "\n" 
 ; List.app 
      (fn (desc, test) =>
          if test (parseInt zip) 
             then print ("   Zip "^zip^" has property: "^desc^"\n") 
          else ())
      (!storedZipTests)
 ; List.app
      (fn (state, area) =>
          List.app
             (fn (desc, test) =>
                 if test (state, area)
                    then print ("   In rate area "^state^"-"^
                                IntInf.toString area^" with property: "^
                                desc^"\n")
                 else ())
             (!storedTests))
      rateareas )
end

val () = app (process o hd) slcsp

end
